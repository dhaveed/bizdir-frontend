import { Component, OnInit } from '@angular/core';
import { ResourceService } from 'src/app/services/resource.service';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  searchForm: FormGroup;
  submitted = false;
  loading = false;
  statusMessage = "";

  result = '';

  constructor(private api: UserService, private formBuilder: FormBuilder, private router: Router) { }

  createForm() {
    this.searchForm = this.formBuilder.group({
      term: ['']
    });
  }

  doSearch(){
    let t = this.searchForm.controls.term.value
    this.submitted = true;
    if (this.searchForm.invalid) {
        return;
    }

    this.loading = true;

    this.api.search(t).subscribe(
      data => {
        if(data.length > 0){
           this.result = data
          this.statusMessage = "Showing " + data.length + " results for " + t;
        } else {
          this.result = "";
          this.statusMessage = "No result found for " + t;
        }
      },
      error=> {
        this.result = '';
        Swal.fire({
          title: 'Error!',
          text: error.error.message,
          type: 'error',
          confirmButtonText: 'Okay'
        })
      }
    )

  }

  viewBiz(biz){
     this.router.navigate(['/single-result', biz.id])
  }

  ngOnInit() {
    this.searchForm = this.formBuilder.group({
      term: ['', Validators.required]
  });
    
  }

}
