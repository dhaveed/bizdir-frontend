import { Component, OnInit } from '@angular/core';
import { Router, Route, ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-view-single',
  templateUrl: './view-single.component.html',
  styleUrls: ['./view-single.component.css']
})
export class ViewSingleComponent implements OnInit {

  public businessId;
  public business;

  constructor(private route: ActivatedRoute, private api: UserService, private router: Router) { }

  ngOnInit() {
    let id = parseInt(this.route.snapshot.paramMap.get('id'));
    this.businessId = id;

    this.api.fetchSingle(this.businessId).subscribe(
      data => {
           this.business = data;
           console.log(this.business);
      },
      error=> {
        this.business = '';
        Swal.fire({
          title: 'Error!',
          text: error.error.message,
          type: 'error',
          confirmButtonText: 'Okay'
        })
        this.router.navigate(['/search'])
      }
    )
    
  }

}
