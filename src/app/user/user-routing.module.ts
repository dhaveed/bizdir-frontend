import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchComponent } from './search/search.component';
import { SearchResultComponent } from './search-result/search-result.component';
import { ViewSingleComponent } from './view-single/view-single.component';


const userRoutes: Routes = [
  {
    path: 'search',
    component: SearchComponent
  },
  {
    path: 'search-result',
    component: SearchResultComponent
  },
  {
    path: 'single-result/:id',
    component: ViewSingleComponent
  }
];

@NgModule({
  imports: [
      RouterModule.forChild(userRoutes)
    ],
  exports: [RouterModule]
})
export class UserRoutingModule { }
