import { TestBed } from '@angular/core/testing';

import { BizfinderService } from './bizfinder.service';

describe('BizfinderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BizfinderService = TestBed.get(BizfinderService);
    expect(service).toBeTruthy();
  });
});
