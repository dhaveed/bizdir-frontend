import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddNewComponent } from './admin/add-new/add-new.component';
import { ViewAllComponent } from './admin/view-all/view-all.component';
import { RequestComponent } from './admin/request/request.component';
import { AccountComponent } from './admin/account/account.component';
import { LoginComponent } from './admin/login/login.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
  {
    path: 'add-new',
    component: AddNewComponent
  },
  {
    path: 'view-all',
    component: ViewAllComponent
  },
  {
    path: 'requests',
    component: RequestComponent
  },
  {
    path: 'account',
    component: AccountComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: "**",
    redirectTo: '/404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
