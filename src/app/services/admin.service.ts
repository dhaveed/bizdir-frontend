import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
 
import { User } from '../admin/login/user';
 
@Injectable({ providedIn: 'root' })
export class AdminService {
  constructor(private http: HttpClient) { }
 
  register(user: User) {
    return this.http.post(`api/register`, user);
  }
 
}
 