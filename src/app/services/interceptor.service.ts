import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class InterceptorService implements HttpInterceptor {
    intercept(
        req: HttpRequest < any > ,
        next: HttpHandler
    ): Observable < HttpEvent < any >> {
        let reqUrl = "http://localhost:8000/api/";
        req = req.clone({
            headers: req.headers.set(
                "Authorization",
                "Bearer " + JSON.parse(localStorage.getItem('currentUser')).token
            ),
            url: reqUrl + "" + req.url
        });
        return next.handle(req);
    }
}