import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../admin/login/user';

  // isAuthenticated(){
  //   return true;
  // }

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {
  }

  search(term: string) {
    return this.http.get < any > (`search/` + term)
    .pipe(map(result => {
        return result;
    }));
  }

  fetchSingle(id: number){
    return this.http.get < any > (`business/` + id)
    .pipe(map(result => {
        console.log(result);
        return result;
    }));
  }
}