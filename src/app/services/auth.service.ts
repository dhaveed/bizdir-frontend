import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../admin/login/user';

  // isAuthenticated(){
  //   return true;
  // }

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private currentUserSubject: BehaviorSubject < User > ;
  public currentUser: Observable < User > ;

  constructor(private http: HttpClient) {
      this.currentUserSubject = new BehaviorSubject < User > (JSON.parse(localStorage.getItem('currentUser')));
      this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
      return this.currentUserSubject.value;
  }

  login(email: string, password: string) {
    return this.http.post < any > (`login`, {
        email, password
    })
    .pipe(map(user => {
        if (user && user.token) {
            // store user details in local storage to keep user logged in
            localStorage.setItem('currentUser', JSON.stringify(user));
            this.currentUserSubject.next(user);
        }

        return user;
    }));
  }

  logout() {
      // remove user data from local storage for log out
      let token = localStorage.getItem('currentUser');
      this.http.get(`logout?token=` + token);
      localStorage.removeItem('currentUser');
      this.currentUserSubject.next(null);
  }

  isAuthenticated(){
    let token = JSON.parse(localStorage.getItem('currentUser')).token;
    if(token != null){
      return true;
    } else {
      return false;
    }
  }

  getUserInfo(){
    let token = JSON.parse(localStorage.getItem('currentUser')).token;
    return this.http.get(`user?token=`+ token);
  }

  save(name, industry, location, phone, email, about, employees){
    let token = JSON.parse(localStorage.getItem('currentUser')).token;
    return this.http.post < any > (`add?token=` + token, {
        name, industry, location, phone, email, about, token, employees
    })
    .pipe(map(response => {
        return response;
    }));
  }

  getAllBiz(){
    let token = JSON.parse(localStorage.getItem('currentUser')).token;
    return this.http.get < any > (`businesses?token=` + token,)
    .pipe(map(response => {
        return response;
    }));
  }
}