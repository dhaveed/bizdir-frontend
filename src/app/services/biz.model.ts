export class Biz {
    name: string;
    location: string;
    industry: string;
    email: string;
    phone: string;
    about: string;
    employees: string;
}