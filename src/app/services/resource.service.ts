import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const headers = new HttpHeaders({'Content-Type': 'application/json'});

@Injectable({
  providedIn: 'root'
})
export class ResourceService {
  apiUrl = "http://localhost:8000/api/";

  constructor(private http: HttpClient) { }

  getAllBiz() {
    return this
            .http
            .get(`${this.apiUrl}/viewbusiness?api_token=CY1GubQdDfvfdvvwiTWtpkacs3O4xF7auWBBDrJD59DL1U7vRXvQkZoBtfDt`, {headers});
  }

  getOneBiz(id){
    return this
    .http.get(`${this.apiUrl}/viewonebusiness?api_token=CY1GubQdDfvfdvvwiTWtpkacs3O4xF7auWBBDrJD59DL1U7vRXvQkZoBtfDt&id=` + id, {headers});
  }
  
  // getOneBiz(id){
  //   return this
  //   .http.get(`${this.apiUrl}/viewonebusiness?api_token=CY1GubQdDfvfdvvwiTWtpkacs3O4xF7auWBBDrJD59DL1U7vRXvQkZoBtfDt&id=` + id, {headers});
  // }

  addNewBiz(id){
    return this
    .http.post(`${this.apiUrl}/viewonebusiness?api_token=CY1GubQdDfvfdvvwiTWtpkacs3O4xF7auWBBDrJD59DL1U7vRXvQkZoBtfDt&id=` + id, {headers});
  }
}
