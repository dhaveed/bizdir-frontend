import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-new',
  templateUrl: './add-new.component.html',
  styleUrls: ['./add-new.component.css']
})
export class AddNewComponent implements OnInit {

  addForm: FormGroup;
  loading = false;
  submitted = false;
  user;
  // returnUrl: string;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthService
   ) { }

  ngOnInit() {
    this.addForm = this.formBuilder.group({
        name: ['', Validators.required],
        industry: ['', Validators.required],
        location: ['', Validators.required],
        phone: ['', Validators.required],
        email: ['', Validators.required],
        about: ['', Validators.required],
        employees: ['', Validators.required]
    });

    if(!this.authenticationService.isAuthenticated()){
      this.router.navigate(['/login'])
    } else {
      console.log("here");
      this.authenticationService.getUserInfo().subscribe(
        data=>{
          this.user = data;
          console.log(data);
        },
        error=>{
          console.log(error);
        }
      );
      return;
    };
    
  }

  get fval(){ //getter method
    return this.addForm.controls;
  }

  saveBusiness(){
    console.log(this.fval);
    this.submitted = true;
    if (this.addForm.invalid) {
        return;
    }
    this.loading = true;

    this.authenticationService.save(this.fval.name.value, this.fval.industry.value, this.fval.location.value, this.fval.phone.value, this.fval.email.value, this.fval.about.value, this.fval.employees.value)
      .subscribe(
        data => {
          console.log(data)
          Swal.fire({
            title: 'Success!',
            text: "Added Business Successfully",
            type: 'success',
            confirmButtonText: 'Okay'
          })
        },
        error => {
          console.log(error);
            Swal.fire({
              title: 'Error!',
              text: error.error.message,
              type: 'error',
              confirmButtonText: 'Okay'
            })
            this.loading = false;
        });
  }
}