import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddNewComponent } from './add-new/add-new.component';
import { ViewAllComponent } from './view-all/view-all.component';
import { RequestComponent } from './request/request.component';
import { AccountComponent } from './account/account.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NotFoundComponent } from '../not-found/not-found.component';
import { AuthGuard } from '../guards/auth-guard.service';


const adminRoutes: Routes = [
    {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'add-new',
        component: AddNewComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'view-all',
        component: ViewAllComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'requests',
        component: RequestComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'account',
        component: AccountComponent,
        canActivate: [AuthGuard]
      },
      {
        path: '404',
        component: NotFoundComponent
      }
];

@NgModule({
  imports: [
      RouterModule.forChild(adminRoutes)
    ],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
