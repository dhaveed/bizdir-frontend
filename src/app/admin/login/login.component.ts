import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import {User } from './user';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  constructor(
      private formBuilder: FormBuilder,
      private route: ActivatedRoute,
      private router: Router,
      private authenticationService: AuthService
  ) {}

  ngOnInit() {
      this.loginForm = this.formBuilder.group({
          email: ['', Validators.required],
          password: ['', Validators.required]
      });

  }

  // for accessing to form fields
  get fval() {
      return this.loginForm.controls;
  }

  doLogin() {
    this.submitted = true;
    if (this.loginForm.invalid) {
        return;
    }

    this.loading = true;
    this.authenticationService.login(this.fval.email.value, this.fval.password.value)
      .subscribe(
        data => {
          Swal.fire({
            title: 'Success!',
            text: "Login Successful",
            type: 'success',
            confirmButtonText: 'Okay'
          })
            this.router.navigate(['/dashboard']);
        },
        error => {
            Swal.fire({
              title: 'Error!',
              text: error.error.message,
              type: 'error',
              confirmButtonText: 'Okay'
            })
            this.loading = false;
        });
  }
}
