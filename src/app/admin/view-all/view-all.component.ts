import { Component, OnInit } from '@angular/core';
import { ResourceService } from 'src/app/services/resource.service';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-view-all',
  templateUrl: './view-all.component.html',
  styleUrls: ['./view-all.component.css']
})
export class ViewAllComponent implements OnInit {

  public user;
  public allBiz;

  constructor(private authenticationService: AuthService, private router: Router) { }

  ngOnInit(){
    if(!this.authenticationService.isAuthenticated()){
      this.router.navigate(['/login'])
    } else {
      console.log("here");
      this.authenticationService.getUserInfo().subscribe(
        data=>{
          this.user = data;
          console.log(data);
        },
        error=>{
          console.log(error);
        }
      );
      // return;
    };

    this.authenticationService.getAllBiz()
    .subscribe(
      data=> {
        this.allBiz = data;
        console.log(data)
      },
      error=>{
        Swal.fire({
          title: 'Error!',
          text: error.error.message,
          type: 'error',
          confirmButtonText: 'Okay'
        })
      }
    );
    
  }

  doLogout() {
    console.log('logging out')
      this.authenticationService.logout();
      this.router.navigate(['/login']);
  }

}
