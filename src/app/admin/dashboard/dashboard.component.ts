import { Component, OnInit, ComponentFactoryResolver, NgModuleRef } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public currentUser;
  public user;
  public allBiz;

  constructor(private resolver: ComponentFactoryResolver, private authenticationService: AuthService, private router: Router) {
    
    this.currentUser = localStorage.getItem('currentUser')? JSON.parse(localStorage.getItem('currentUser')) : '';

    const factory = resolver.resolveComponentFactory(DashboardComponent);
    // console.log('the factory:', factory);
  
    // Access the private ngModule property.
    const ngModuleRef: NgModuleRef<any> = (factory as any).ngModule;
    let modName = ngModuleRef.instance.constructor.name;
    console.log('the module name:', modName)
  }

  ngOnInit(){
    if(!this.authenticationService.isAuthenticated()){
      this.router.navigate(['/login'])
    } else {
      console.log("here");
      this.authenticationService.getUserInfo().subscribe(
        data=>{
          this.user = data;
          console.log(data);
        },
        error=>{
          console.log(error);
        }
      );
      // return;
    };

    this.authenticationService.getAllBiz()
    .subscribe(
      data=> {
        this.allBiz = data;
        console.log(data)
      },
      error=>{
        Swal.fire({
          title: 'Error!',
          text: error.error.message,
          type: 'error',
          confirmButtonText: 'Okay'
        })
      }
    );
    
  }

  doLogout() {
    console.log('logging out')
      this.authenticationService.logout();
      this.router.navigate(['/login']);
  }

}
