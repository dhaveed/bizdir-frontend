import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ViewAllComponent } from './view-all/view-all.component';
import { AddNewComponent } from './add-new/add-new.component';
import { AccountComponent } from './account/account.component';
import { RequestComponent } from './request/request.component';
import { AuthComponent } from './auth/auth.component';
import { AdminRoutingModule } from './admin-routing.module';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from '../guards/auth-guard.service';
import { ReactiveFormsModule, FormBuilder } from '@angular/forms';
// import { SidebarComponent } from '../sidebar/sidebar.component';
// import { HeaderComponent } from '../header/header.component';



@NgModule({
  declarations: [DashboardComponent, ViewAllComponent, AddNewComponent, AccountComponent, RequestComponent, AuthComponent, LoginComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AdminRoutingModule
  ],
  providers: [
    AuthGuard
  ]
})
export class AdminModule { }
